![](https://gitlab.com/Antoniii/monte-karlo-van-python/-/raw/main/test-prog.PNG)

## Sources

* [Моделирование методом Монте-Карло](http://sewiki.ru/%D0%9C%D0%BE%D0%B4%D0%B5%D0%BB%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5_%D0%BC%D0%B5%D1%82%D0%BE%D0%B4%D0%BE%D0%BC_%D0%9C%D0%BE%D0%BD%D1%82%D0%B5-%D0%9A%D0%B0%D1%80%D0%BB%D0%BE)
* [Вычисление π: моделирование методом Монте-Карло](https://medium.com/nuances-of-programming/%D0%B2%D1%8B%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%CF%80-%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BC%D0%B5%D1%82%D0%BE%D0%B4%D0%BE%D0%BC-%D0%BC%D0%BE%D0%BD%D1%82%D0%B5-%D0%BA%D0%B0%D1%80%D0%BB%D0%BE-3314fd01bbbe)
* [Монте-Карло в Python](https://pythobyte.com/monte-carlo-in-python-37e44379/)
* [arkaprabha-majumdar / monte-carlo ](https://github.com/arkaprabha-majumdar/monte-carlo)
